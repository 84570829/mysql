##### 项目名称：swoole协程mysql连接池
##### 版本特性：
- 自动回收连接
- 自动连接保活
- 自动断线重连
- 支持集群分布
- 支持读写分离
- 集成连贯操作
##### 文档地址：
https://www.kancloud.cn/samir/swoole-mysql/1413714

	
	
